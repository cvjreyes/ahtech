# README #

Existen dos cuentas de usuario:

cv.jreyes@gmail.com
rafael.samano@aheadtechnology.es

Ambas con contraseña: 123456

### Querys ###
En el controlador están los tres querys ejecutados: app\Http\Controllers\HomeController.php

### La Base de datos: ###

El fichero .sql se encuentra en la raiz ..\banco_db

### Configuración del Vhosts si usas XAMPP o similar ###

<VirtualHost localhost:8096>
  DocumentRoot "C:\xampp\htdocs\AHTECH\public"
		SetEnv DB_Host "127.0.0.1"
		SetEnv DB_DATABASE "banco_db"
		SetEnv DB_USERNAME "root"
		SetEnv DB_PASSWORD ""
  ServerAdmin technipfmc.app
  <Directory "C:\xampp\htdocs\AHTECH">
		Options Indexes FollowSymLinks
        AllowOverride All
        Require all granted
  </Directory>
</VirtualHost>

(8096 es el puerto que seleccioné, puedes configurar como desees)
